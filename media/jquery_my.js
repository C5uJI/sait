jQuery(document).ready(function(){
    $(function(){
        $("#test").click(function(){
            var str = $("#coment_form").formSerialize();

            $.ajax({
                type: 'POST',
                url: window.location.pathname,
                data: str,
                dataType: 'text',
                success: function(data){
                    $('.error').remove();
                    var coment = {};
                    JSON.parse(data, function (key, value) {
                        coment[key] = value;
                        });

                    if (coment['result'] == 'sucsess') {
                        var dat=new Date(coment.date_coment)
                        var options = {
                            month: "short", day: "numeric", year: "numeric", hour: "2-digit", minute: "2-digit"
                        };

                        var margin_left = coment.html_level
                        if (margin_left >= 300){
                            margin_left = coment.html_level_full
                        };

                        var coment_html = ('<div class="coment" style="margin:0 0 20px ' + margin_left +'px" id="coment_' + coment.id +'">'+ coment.autor_coment +':<div style="padding-left: 50px">'+ coment.coment_text +'</div><div>' + dat.toLocaleTimeString("en-us", options) +'<span> | </span> <a href="#metka" onclick="value_parent(' + coment.id +')">ответить</a></div></div>')
                        if (coment.html_level == 0) {
                            if ($("#all_coments").last()) {
                                $("#all_coments").last().after(coment_html);
                            } else {
                                $("#all_coments").html(coment_html);
                            };

                        } else {
                            $("#coment_" + coment.parent_id).after(coment_html);
                        };
                        $('#id_parent_id').val('');
                        $('#id_message').val('');
                        $('#coment_form').before('Ваш коментарий отправлен, спасибо')
                    }
                    else if (coment['result'] == 'error') {
                        for (var k in coment) {
                            $('input[name=' + k + ']').after('<span class="error" style="margin-left: 20px; color: red">' + coment[k] + '</span>');
                            $('textarea[name=' + k + ']').after('<span class="error" style="margin-left: 20px; color: red" >' + coment[k] + '</span>');
                        }
                    }
                }
            });
        });

        $(function() {
            var url=document.location.href;
            $.each($('#templatemo_menu a'),function(){
                if (~url.indexOf(this.href)){
                    $(this).addClass('current');
                };
            });
        });



    });
});






