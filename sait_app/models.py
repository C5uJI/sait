# -*- coding: utf-8 -*-
from django.db import models


class Article(models.Model):
    article_title = models.CharField(max_length=255, verbose_name="Заголовок")
    article_text = models.TextField(max_length=1000, verbose_name="Статья")
    date_publication = models.DateField(blank=True, null=True, verbose_name="дата публикации")

    def __unicode__(self):
        return '%s' % (self.article_title)


class Coments(models.Model):
    autor_coment = models.CharField(max_length=20, verbose_name="Имя")
    mail_autor_coment = models.EmailField(verbose_name="Почта")
    coment_text = models.TextField(max_length=1000, verbose_name="Коментарий")
    date_coment = models.DateTimeField(auto_now=True)
    article = models.ForeignKey(Article)
    parent = models.ForeignKey('self', blank=True, null=True)
    path = models.TextField(max_length=800, blank=True)

    def __unicode__(self):
        return '%s' % (self.autor_coment)

    def save(self):
        super(Coments, self).save()
        if not self.path:
            if self.parent:
                try:
                    parentt = self.parent
                    parent_path = parentt.path
                except:
                    parent_path = ''
                self.path = '%s%08d' % (parent_path, self.id)
            else:
                self.path = '%08d' % self.id
            super(Coments, self).save()

    @property
    def level(self):
        return max(0, (len(self.path))/8)

    @property
    def html_level(self):
        return (self.level - 1) * 20

    @property
    def html_level_full(self):
        return (self.html_level - 300) / 2
