# -*- coding: utf-8 -*-
from django import forms


class ComentsForm(forms.Form):
    my_default_errors = {
        'required': 'Это поле не должно быть пустым',
        'invalid': 'Введите корректную почту'
    }

    parent_id = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    message = forms.CharField(error_messages=my_default_errors, widget=forms.Textarea())
    email = forms.EmailField(error_messages=my_default_errors)
    name = forms.CharField(error_messages=my_default_errors, max_length=20)



