# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('article_title', models.CharField(max_length=255, verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb3\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2\xd0\xbe\xd0\xba')),
                ('article_text', models.TextField(max_length=1000, verbose_name=b'\xd0\xa1\xd1\x82\xd0\xb0\xd1\x82\xd1\x8c\xd1\x8f')),
                ('date_publication', models.DateField(null=True, verbose_name=b'\xd0\xb4\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xbf\xd1\x83\xd0\xb1\xd0\xbb\xd0\xb8\xd0\xba\xd0\xb0\xd1\x86\xd0\xb8\xd0\xb8', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Coments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('autor_coment', models.CharField(max_length=20, verbose_name=b'\xd0\x98\xd0\xbc\xd1\x8f')),
                ('mail_autor_coment', models.EmailField(max_length=254, verbose_name=b'\xd0\x9f\xd0\xbe\xd1\x87\xd1\x82\xd0\xb0')),
                ('coment_text', models.TextField(max_length=1000, verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xbc\xd0\xb5\xd0\xbd\xd1\x82\xd0\xb0\xd1\x80\xd0\xb8\xd0\xb9')),
                ('date_coment', models.DateTimeField(auto_now=True)),
                ('path', models.TextField(max_length=800, blank=True)),
                ('article', models.ForeignKey(to='sait_app.Article')),
                ('parent', models.ForeignKey(blank=True, to='sait_app.Coments', null=True)),
            ],
        ),
    ]
