# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from sait_app.models import Article,  Coments
from sait_app.form import ComentsForm
from django.template import RequestContext
from django.db.models import Count
from django.shortcuts import get_object_or_404
from django.http import Http404, JsonResponse, HttpResponseRedirect
import datetime


def all_paginator(request, paginator):
    page = request.GET.get('page')

    try:
        any_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        any_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        any_page = paginator.page(paginator.num_pages)
    return {'page': any_page}


def page_article(request):

    now = datetime.date.today()
    ordering = request.GET.get("sort", "date_publication")
    valid_ordering = {"coments__count", "date_publication", "-coments__count", "-date_publication"}
    ordering = ordering if ordering in valid_ordering else "date_publication"
    queryset = Article.objects.filter(date_publication__lte=now).annotate(Count('coments')).order_by(ordering)
    paginator = Paginator(queryset, 4)
    context = all_paginator(request, paginator)
    context.update({'ordering': ordering})

    return render_to_response('blog.html', context)


def detail(request, id_article):
    today = datetime.date.today()
    try:
        id_article = int(id_article)
    except ValueError:
        raise Http404()
    queryset = Article.objects.filter(date_publication__lte=today)
    article_detailed = get_object_or_404(queryset, pk=id_article)
    article_coments = Coments.objects.filter(article=id_article, path__regex=r"^\d{8,120}$").order_by('path')
    if request.method == 'POST':
        form = ComentsForm(request.POST)
        if form.is_valid():
            parent_id = form.cleaned_data.get('parent_id')
            email = form.cleaned_data.get('email')
            message = form.cleaned_data.get('message')
            name = form.cleaned_data.get('name')
            new_coment = Coments(
                article_id=id_article,
                autor_coment=name,
                coment_text=message,
                mail_autor_coment=email
            )
            add_coment = {}
            if parent_id:
                new_coment.parent = Coments.objects.get(id=parent_id)
                add_coment["parent_id"] = new_coment.parent.id
                add_coment["path"] = new_coment.parent.path
            new_coment.save()
            add_coment.update({"id": new_coment.id,
                               "autor_coment": new_coment.autor_coment,
                               "date_coment": new_coment.date_coment,
                               "coment_text": new_coment.coment_text,
                               "mail_autor_coment": new_coment.mail_autor_coment,
                               "html_level": new_coment.html_level,
                               })
            return JsonResponse({'comentarii': add_coment, "result": "sucsess"})
        errors = {}
        for k in form.errors:
            errors[k] = form.errors[k][0]

        return JsonResponse({'erors': errors, 'result': 'error'})
    else:
        form = ComentsForm()

    paginator = Paginator(article_coments, 20)
    context = all_paginator(request, paginator)
    context.update({'article_detailed': article_detailed, 'form': form})
    return render_to_response('detail_article.html', context, context_instance=RequestContext(request))


def detail_coment(request, path, id_article):
    queryset = Coments.objects.filter(path__regex=r"^%s" % path).order_by('path')
    paginator = Paginator(queryset, 50)
    context = all_paginator(request, paginator)

    if request.method == 'POST':
        form = ComentsForm(request.POST)
        if form.is_valid():
            parent_id = form.cleaned_data.get('parent_id')
            email = form.cleaned_data.get('email')
            message = form.cleaned_data.get('message')
            name = form.cleaned_data.get('name')
            new_coment = Coments(
                article_id=id_article,
                autor_coment=name,
                coment_text=message,
                mail_autor_coment=email
            )
            add_coment = {}
            if parent_id:
                new_coment.parent = Coments.objects.get(id=parent_id)
                add_coment["parent_id"] = new_coment.parent.id
                add_coment["path"] = new_coment.parent.path
            new_coment.save()
            add_coment.update({"id": new_coment.id,
                               "autor_coment": new_coment.autor_coment,
                               "date_coment": new_coment.date_coment,
                               "coment_text": new_coment.coment_text,
                               "mail_autor_coment": new_coment.mail_autor_coment,
                               "html_level": new_coment.html_level,
                               "html_level_full": new_coment.html_level_full,
                               })
            return JsonResponse({'comentarii': add_coment, "result": "sucsess"})
        errors = {}
        for k in form.errors:
            errors[k] = form.errors[k][0]

        return JsonResponse({'erors': errors, 'result': 'error'})
    else:
        form = ComentsForm()
    context.update({'form': form})
    return render_to_response('coments.html', context, context_instance=RequestContext(request))


def page_home(request):
    return render_to_response('base.html')


def page_start(request):
    return HttpResponseRedirect(r'home/')


def page_about_me(request):
    return render_to_response('about_me.html')


def page_contacts(request):
    return render_to_response('contacts.html')