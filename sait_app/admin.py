# -*- coding: utf-8 -*-
from django.contrib import admin
from sait_app.models import Article, Coments


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('article_title', 'article_text', 'date_publication', 'id')


class ComentsAdmin(admin.ModelAdmin):
    list_display = ('autor_coment', 'coment_text', 'date_coment', 'mail_autor_coment', 'id',\
                    'parent', 'path', 'article')
    list_per_page = 500


admin.site.register(Article, ArticleAdmin)
admin.site.register(Coments, ComentsAdmin)
# Register your models here.
