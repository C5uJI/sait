from django.core.management.base import NoArgsCommand
from sait_app.models import Coments, Article
import random
import datetime
import pytz


class Command(NoArgsCommand):

    def handle_noargs(self, **options):
        file = open('sait_app/text.txt', 'r')
        id_coment = Coments.objects.all().count() + 1
        id_article = id_article_start = int(Article.objects.all().count() + 1)
        list1 = []
        tz = pytz.timezone('Europe/Minsk')

        for article in xrange(1):
            new_article = Article(
                id=id_article,
                article_title=file.read(random.randint(1, 100)),
                article_text=file.read(random.randint(100, 500)),
                date_publication=datetime.date(2015, 10, random.randint(1, 30))
            )
            list1.append(new_article)
            id_article += 1
            if article % 100 == 0:
                Article.objects.bulk_create(list1)
                list1 = []
        Article.objects.bulk_create(list1)

        for i in xrange(id_article_start, id_article):
            list1 = []

            for o_coment in xrange(100):
                maill = file.read(random.randint(1, 20)) + '@' + file.read(random.randint(2, 8)) + '.ru'
                maill.replace(' ', '')
                new_coment = Coments(
                    id=id_coment,
                    article_id=i,
                    autor_coment=file.read(random.randint(1, 20)),
                    coment_text=file.read(random.randint(1, 200)),
                    mail_autor_coment=maill,
                    date_coment=datetime.datetime(2015, 10, random.randint(1, 30), random.randint(0, 23), random.randint(0, 59), random.randint(0, 59), tzinfo=tz)
                )
                new_coment = get_path(new_coment)
                list1.append(new_coment)
                id_coment += 1
            Coments.objects.bulk_create(list1)
            list2 = peremen = list1
            list1 = []
            k = 1000

            for v_coment in xrange(20000):
                maill = file.read(random.randint(1, 20)) + '@' + file.read(random.randint(2, 8)) + '.ru'
                maill.replace(' ', '')
                n_coment = Coments(
                    id=id_coment,
                    article_id=i,
                    autor_coment=file.read(random.randint(1, 20)),
                    coment_text=file.read(random.randint(1, 200)),
                    mail_autor_coment=maill,
                    parent=list2[random.randint(0, len(list2)-1)],
                    date_coment=datetime.datetime(2015, 10, random.randint(1, 30), random.randint(0, 23), random.randint(0, 59), random.randint(0, 59), tzinfo=tz)
                )
                if len(n_coment.parent.path) < 800:
                    n_coment = get_path(n_coment)
                    list1.append(n_coment)
                    id_coment += 1
                if (v_coment % k == 0):
                    k -= 50
                    if k < 100:
                        k = 100
                    Coments.objects.bulk_create(list1)
                    list2 = peremen + list1
                    peremen = list1
                    list1 = []

            Coments.objects.bulk_create(list1)

        file.close()
        self.stdout.write('Successfully add coments and article')


def get_path(self):
     if not self.path:
        if self.parent:
            try:
                parentt = self.parent
                parent_path = parentt.path
            except:
                parent_path = ''
            self.path = '%s%08d' % (parent_path, self.id)
        else:
            self.path = '%08d' % self.id
        return self
