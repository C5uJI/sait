from django.conf.urls import url
from sait_app import views


urlpatterns = [
    url(r'^$', views.page_start, name="page_start"),
    url(r'about_me/$', views.page_about_me, name="page_about_me"),
    url(r'contacts/$', views.page_contacts, name="page_contacts"),
    url(r'home/$', views.page_home, name="page_home"),
    url(r'article/$', views.page_article, name="page_article"),
    url(r'article/(\d{1,2})/$', views.detail, name="detail"),
    url(r'media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': './media/'}),
    url(r'^detail_coment/(\d{8,})/(\d{1,})/$', views.detail_coment, name="detail_coment"),
]
